Still nothing to say about.

To test: go directly to /register to register a new user and then 
go to /channels/create to create a new channel (each access creates 
a new channel). After that, create discussions by accessing 
/discussions/create . The channels field in the form could be 
filled with channels's idname (space separated; example: 
channel_0 channel_2).

This file is for you to describe the townsquare application. Typically
you would include information such as the information below:

Installation and Setup
======================

Install ``townsquare`` using easy_install::

    easy_install townsquare

Make a config file as follows::

    paster make-config townsquare config.ini

Tweak the config file as appropriate and then setup the application::

    paster setup-app config.ini

Then you are ready to go.
