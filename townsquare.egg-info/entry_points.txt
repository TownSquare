
    [paste.app_factory]
    main = townsquare.config.middleware:make_app

    [paste.app_install]
    main = pylons.util:PylonsInstaller
    