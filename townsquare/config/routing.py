"""Routes configuration

The more specific and detailed routes should be defined first so they
may take precedent over the more generic routes. For more information
refer to the routes manual at http://routes.groovie.org/docs/
"""
from pylons import config
from routes import Mapper

def make_map():
    """Create, configure and return the routes Mapper"""
    map = Mapper(directory=config['pylons.paths']['controllers'],
                 always_scan=config['debug'])
    map.minimization = False

    # The ErrorController route (handles 404/500 error pages); it should
    # likely stay at the top, ensuring it can always be resolved
    map.connect('/error/{action}', controller='error')
    map.connect('/error/{action}/{id}', controller='error')

    # CUSTOM ROUTES HERE

    # Account
    map.connect('/invite', controller='account', action='invite')
    map.connect('/activate', controller='account', action='activate')
    map.connect('/activate/{key}', controller='account', action='activate')
    map.connect('/register', controller='account', action='register')
    map.connect('/login', controller='account', action='login')
    map.connect('/logout', controller='account', action='logout')

    # Discussion
    map.connect('/post/{idname}/delete', controller='post', action='delete')
    map.connect('/post/{idname}/edit', controller='post', action='edit')
    map.connect('/post/{idname}', controller='post', action='view')
    map.connect('/posts/create', controller='post', action='edit')
    map.connect('/posts', controller='post', action='list')
    
    # Channel
    map.connect('/channel/{idname}', controller='channel', action='view')
    map.connect('/channels/create', controller='channel', action='create')
    map.connect('/channels/pick_one', controller='channel', action='pick_one')
    map.connect('/channels/pick_multi', controller='channel', action='pick_multi')
    map.connect('/channels', controller='channel', action='list')
    
    # User
    map.connect('/user/{idname}', controller='user', action='view')
    map.connect('/users', controller='user', action='list')

    return map
