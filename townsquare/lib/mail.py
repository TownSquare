
import smtplib
from email.mime.text import MIMEText

def send_administration_mail(recepient, subject, message):
  return
  administration_email_username = ''
  administration_email_address = ''
  administration_email_password = ''
  administration_email_host = ''
  administration_email_port = 465
  
  #TODO: mail the activation code
  msg = MIMEText(message)
  
  # me == the sender's email address
  # you == the recipient's email address
  msg['Subject'] = subject
  msg['From'] = administration_email_address
  msg['To'] = recepient
  
  # Send the message via our own SMTP server, but don't include the
  # envelope header.
  s = smtplib.SMTP_SSL(administration_email_host, 465)
  s.login(administration_email_username, administration_email_password)
  s.sendmail(administration_email_address, [recepient], msg.as_string())
  s.quit()
