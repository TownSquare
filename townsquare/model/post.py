import datetime

from fma import SuperDoc
from fma.orm import relation
from townsquare.model.user import User
from townsquare.model.channel import Channel

class Post(SuperDoc):
    _collection_name = 'posts'
    
    @property
    def creator(self):
        #TODO: optimize
        try:
            if self.creator_id == self._creator._id:
                return self._creator
        except: pass
        if not hasattr(self, 'creator_id') or len(self.creator_id) is 0:
            return None
        #TODO: should be something like: account.get_account(creator_id)
        self._creator = self._monga.col(User).find_one(_id=self.creator_id)
        return self._creator
        
    @property
    def url(self):
        return '/post/' + str(self._id)
            
    def get_host(self):
        try:
            if self.host_classname == self.host.__class__.__name__  and self.host_id == self.host._id:
                return self.host
        except: pass
        if not hasattr(self, 'host_classname') or len(self.host_classname) is 0:
            return None
        if not hasattr(self, 'host_id') or len(self.host_id) is 0:
            return None
        cl = globals()[self.host_classname]
        self.host = self._monga.col(cl).find_one(_id=self.host_id)
        return self.host
    
    def get_parent(self):
        #TODO: check for loop
        #TODO: optimize
        try:
            if self.parent_id == self.parent._id:
                return self.parent
        except: pass
        if not hasattr(self, 'parent_id') or len(self.parent_id) is 0:
            return None
        self.parent = self._monga.col(Post).find_one(_id=self.parent_id)
        return self.parent
    
    def direct_response_count(self):
        try: return self._dresp_count
        except: pass
        return 0
    def total_response_count(self):
        try: return self._tresp_count
        except: pass
        return 0
    
    def get_short_content(self, max_len=100):
        #TODO: strip the markup
        pass
        
    def _respon_created(self, user_id, timestamp=None, update_stats=False, direct=False):
        if timestamp is None:
            timestamp = datetime.datetime.utcnow()
        if direct:
            self.dresp_user_id = user_id
            self.dresp_timestamp = timestamp
        else:
            self.iresp_user_id = user_id
            self.iresp_timestamp = timestamp
        if update_stats:
            #CHECK: saved?
            self._dresp_count = self._monga.col(Post).find(parent_id=self._id).count()
            #TODO: calculate total children (including grand children recursively)
        try:
            self.get_parent()._respon_created(user_id, timestamp, update_stats, False)
        except: pass
        self.save()
    
    def delete(self, deleter_id, reason=None, timestamp=None):
        #TODO: check if already deleted
        self.deletion_user_id = deleter_id
        if timestamp is None:
            self.deletion_timestamp = datetime.datetime.utcnow()
        else:
            self.deletion_timestamp = timestamp
        if reason and len(reason):
            self.deletion_reason = reason
        self.deletion_status = True
        #TODO
        #self.deletion_count = 
        self.save()
    
    def save(self):
        #TODO: check attributes
        #if self.name is None or len(self.name) is 0:
            #TODO: take the name from the few first words of the content (or bail out?)
        #    pass
        #if self.idname is None or len(self.idname) is 0:
            #TODO: generate idname (case-insensitive unique url-safe name) from title (name)
            # if the idname is not provided generate it from name
            # if the generated idname conflicts with existing one, add serial (dunno how to track the existing serials)
            # if the idname is provided but that idname is already exists, bail out
        #    pass
        
        #TODO: limit content
        
        if hasattr(self, 'host'):
            #TODO: check the validity of the object (e.g. has a valid id)
            if isinstance(self.host, SuperDoc):
                self.host_classname = self.host.__class__.__name__
                self.host_id = str(self.host._id)
            else:
                #TODO: raise error
                pass
        else:
        #    if hasattr(self, 'host_classname'):
        #        self.host_classname = '' #TODO: delete property
        #    if hasattr(self, 'host_id'):
        #        self.host_id = '' #TODO: delete property
            pass
        
        if hasattr(self, 'parent'):
            if not isinstance(self.parent, Post):
                #TODO: raise error
                pass
            #TODO: check validity
            self.parent_id = str(self.parent._id)
        #    pop = self.parent
        #    self.parent_path = []
        #    while pop is not None:
        #        self.parent_path.append(str(pop._id))
        #        pop = pop.get_parent()
        else:
            #TODO: delete property
            pass
            
        saved = self._saved()
        timestamp = datetime.datetime.utcnow()
        
        if not saved:
            if hasattr(self, 'creation_timestamp') is not True or len(self.creation_timestamp) is 0:
                self.creation_timestamp = timestamp
        else:
            #TODO: check whether the content has been modified (diff the content)
            #TODO: save revision
            #self.modification_user_id
            self.modification_timestamp = timestamp
            #self.modification_count = self.modification_count + 1
            pass
                
        super(Post, self).save()
        
        #TODO: need to care about edit
        if not saved:
            try:
                self.get_parent()._respon_created(self.creator_id, timestamp, True, True)
            except: pass
