
from fma import SuperDoc
from fma.orm import relation

#TODO: membership/subscription, member roles
#CHECK: allow markdown in the description?
class Channel(SuperDoc):
    _collection_name = 'channels'
    
    @property
    def url(self):
        return '/channel/' + str(self._id)
        
    def get_short_description(self):
        if not hasattr(self, 'description'):
            return ''
        if len(self.description) < 140:
            return self.description
        #TODO: truncate string
        desc = self.description
        iend = desc.rfind(' ', 0, 137)
        if iend < 0:
            desc = "%s..." % desc[0:137]
        else:
            desc = "%s..." % desc[0:iend]
        #TODO: find the last space or return as-is if no spaces within
        return desc
    
    def save(self):
        #TODO: check attributes
        if self.name is None or len(self.name) is 0:
            #TODO: take the name from the few first words of the description (or bail out?)
            pass
        if self.idname is None or len(self.idname) is 0:
            #TODO: generate idname (case-insensitive unique url-safe name) from title (name)
            # if the idname is not provided generate it from name
            # if the generated idname conflicts with existing one, add serial (dunno how to track the existing serials)
            # if the idname is provided but that idname is already exists, bail out
            pass
        super(Channel, self).save()
