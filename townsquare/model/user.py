import logging, re, datetime, uuid, hashlib
from zlib import crc32

from pylons import session, app_globals as g

from fma import SuperDoc
from fma.orm import relation


def _hash_passkey(passkey, salt):
    #CHECK: use static salt?
    #return hashlib.sha1(salt + hashlib.md5(passkey).hexdigest()).hexdigest()
    return hashlib.sha1(salt + passkey).hexdigest()

def _generate_activation_key():
    #CHECK: more sophisticated generator?
    #return str(uuid.uuid4())
    return '%x' % (crc32(str(uuid.uuid4())) & 0xffffffffL)
    
    
class Error(Exception):
    pass

class NameInvalid(Error): pass
class NameExists(Error): pass
class EmailInvalid(Error): pass
class EmailExists(Error): pass
class UserInvalid(Error): pass
    

class User(SuperDoc):
    _collection_name = 'users'

    idname = unicode

    _opt = {
        'req' : ['idname','email','passkey']
    }
    
    @property
    def id(self):
        return str(self._id)
    @property
    def url(self):
        return self.idname and '/user/' + self.idname or ''
    
    def save(self):
        if not self._saved():
            #TODO: check for existing name or email
            if self.idname is None:
                if self.name is None:
                    raise NameInvalid
                self.idname = self.name.lower()
            else:
                self.idname = self.idname.lower()
            #TODO: name validity check here
            if len(self.idname) < 4 or len(self.idname) > 32:
                raise NameInvalid
            # Check if the id already existed
            #TODO: expiration (when the account activation is expired, the name could be used)
            if self._monga.col(User).find_one(idname=self.idname):
                raise NameExists
            self.email = self.email.lower()
            #TODO: check email address validity here
            # Check if the email address already existed
            if self._monga.col(User).find_one(email=self.email):
                raise EmailExists
            if hasattr(self, 'creation_timestamp') is not True or len(self.creation_timestamp) is 0:
                self.creation_timestamp = datetime.datetime.utcnow()
            self.salt = '%x' % (crc32(str(uuid.uuid4())) & 0xffffffffL)
            self.passkey = _hash_passkey(self.passkey, self.salt)
            #TODO: check config for activation method
            if hasattr(self, 'active') is False or self.active is not True:
                self.active = False
                self.activation_key = _generate_activation_key()
            #self.banned = False
            #if self.admin is None:
            #    self.admin = False
        else:
            #TODO: check for changed properties
            pass
        super(User, self).save()
    
    def activate(self, actkey):
        #TODO: expiration
        if self.activation_key == actkey:
            self.active = True
            #TODO: clear completely
            self.activation_key = None
            return True
        return False
        
    def regenerate_activation_key(self, active_check=True):
        if (active_check and self.activate is not True) or active_check == False:
            self.activation_key = _generate_activation_key()
        
    def authenticate(self, passkey):
        """ Stateless
        """
        if not self._saved():
            return False
        return self.passkey == _hash_passkey(passkey, self.salt)
    
    def change_passkey(self, newpasskey):
        """ WARNING: no old passkey checking (use authenticate first)
        """
        self.passkey = _hash_passkey(newpasskey, self.salt)
        


def get_current_user():
    acc_id = session.get('account.id')
    if acc_id is not None and len(acc_id):
        return g.db.col(User).find_one(_id=acc_id)
    return None

def user_logged_in():
    acc_id = session.get('account.id')
    return acc_id is not None and len(acc_id) is not 0
    
def create_login_url(dest_url):
    #TODO: implement the redirection parameterization
    #TODO: session
    return '/login'

def create_logout_url(dest_url):
    return '/logout'
