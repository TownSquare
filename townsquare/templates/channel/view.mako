<%inherit file="/base.mako"/>

<div>
<h1>${h.literal(c.ch.name)}</h1>
<div>
${h.literal(c.ch.description)}
</div>
</div>

<div>
<!--
${h.secure_form(url('/posts/create'))}
  <div>Post a shout</div>
  <div>${h.textarea(name='content', rows=7, cols=40, content=c.content)}</div>
  <div>
    ${h.submit(value='Post', name='commit')}
    ${h.hidden(name='content_type', value='text')}
    ${h.hidden(name='host_classname', value=c.ch.__class__.__name__)}
    ${h.hidden(name='host_id', value=c.ch._id)}
    ${h.hidden(name='redir', value=c.ch.url)}
  </div>
${h.end_form()}
-->
<ul class="inline-list">
<li><a href="/posts/create?host_classname=Channel&host_id=${c.ch._id}&type=topic">Start a discussion</a></li>
<li><a href="/posts/create?host_classname=Channel&host_id=${c.ch._id}&type=question">Ask a question</a></li>
<li><a href="/posts/create?host_classname=Channel&host_id=${c.ch._id}&type=link">Post a link</a></li>
</ul>
</div>

<div class="wall">
<%
  c.post_list = c.ch.posts
  c.current_url = c.ch.url
%>
<%include file="/post/list-embed.mako"/>
</div>
