<%inherit file="/base.mako"/>

<h1>Channels</h1>

% if c.item_list.count() > 0:
<ul>
% for p in c.item_list:
  <li>
    <div><strong><a href="${p.url}">${p.name}</a></strong></div>
    <div>${p.get_short_description()}</div>
  </li>
% endfor
</ul>
% endif
