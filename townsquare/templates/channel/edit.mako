<%inherit file="/base.mako"/>

<h1>Create A New Channel</h1>

<div>
${h.secure_form(url(''))}
<dl>
  <dt>Name</dt>
  <dd>
    ${h.text(name='title', value=c.title, maxLength=255)}
  </dd>
  <dt>Description</dt>
  <dd>
    ${h.textarea(name='content', rows=7, cols=40, content=c.content)} <br />
  </dd>
  <dt>&nbsp;</dt>
  <dd>
    ${h.submit(value='Save changes', name='commit')}
  </dd>
</dl>
${h.end_form()}
</div>
