<%inherit file="/base.mako"/>\

<%def name="header()">Editing ${c.title}</%def>

<h1>Account Activation</h1>

${h.secure_form(url(''))}
<dl>

  <dt>Activation Code</dt>
  <dd>
    ${h.text(name='activation_key', value=c.activation_key, maxLength=30)}
%if 'activation_key' in c.field_errors:
    <span class="field-error">*</span>
%endif
    <br/><small><em>Enter your account activation code.</em></small>
  </dd>

  <dt>&nbsp;</dt>
  <dd>${h.submit(value='Submit', name='commit')}</dd>

</dl>
${h.end_form()}

<div>${c.errors}</div>
