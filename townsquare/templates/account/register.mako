<%inherit file="/base.mako"/>\

<%def name="header()">Editing ${c.title}</%def>

<h1>Account Registration</h1>

<div>All fields are required.</div>

${h.secure_form(url(''))}
<dl>

  <dt>Username</dt>
  <dd>
    ${h.text(name='username', value=c.username, maxLength=30)}
%if 'username' in c.field_errors:
    <span class="field-error">*</span>
%endif
    <br/><small><em>Will be your login id. Use only alphabets, numerics and underscores with length between 4 and 25 characters.</em></small>
  </dd>

  <dt>Email Address</dt>
  <dd>
    ${h.text(name='email', value=c.email, maxLength=250)}
%if 'email' in c.field_errors:
    <span class="field-error">*</span>
%endif
    <br/><small><em>Enter your active email address. We'll use this as administration contact including account activation and password reset.</em></small>
  </dd>
  <dd>
    ${h.text(name='email_confirm', value=c.email_confirm, maxLength=250)}
%if 'email_confirm' in c.field_errors:
    <span class="field-error">*</span>
%endif
    <br/><small><em>Re-enter your email address to ensure that you have entered it correctly.</em></small>
  </dd>

  <dt>Password</dt>
  <dd>${h.password(name='password', maxLength=64)}
%if 'password' in c.field_errors:
    <span class="field-error">*</span>
%endif
  <br/><small><em>Minimum of 6 characters long. Use variations of alphabets, numbers and symbols.</em></small>
  </dd>
  <dd>
    ${h.password(name='password_confirm', maxLength=64)}
%if 'password_confirm' in c.field_errors:
    <span class="field-error">*</span>
%endif
    <br/><small><em>Re-enter your password to ensure that you have entered it correctly.</em></small>
  </dd>

  <dt>&nbsp;</dt>
  <dd>${h.submit(value='Submit', name='commit')}</dd>

  <dt>&nbsp;</dr>
  <dd>
    <p>If your account needs activation and you've got the activation key, you can activate it on the <a href="${url('/activate')}">activation page</a>.
    And if your account already active, you can login on the <a href="${url('/login')}">login page</a>.
    <p>By registering, you agree to the Terms of Service and Privacy Policy.
  </dd>

</dl>
${h.end_form()}

<div>${c.errors}</div>
