<%inherit file="/base.mako"/>\

<h1>Login</h1>

${h.secure_form(url(''))}
<dl>

  <dt>Username</dt>
  <dd>
    ${h.text(name='username', value=c.username, maxLength=30)}
%if 'username' in c.field_errors:
    <span class="field-error">*</span>
%endif
  </dd>

  <dt>Password</dt>
  <dd>${h.password(name='password', maxLength=64)}
%if 'password' in c.field_errors:
    <span class="field-error">*</span>
%endif
  </dd>

  <dt>&nbsp;</dt>
  <dd>${h.submit(value='Submit', name='commit')}</dd>

</dl>
${h.end_form()}

<div>${c.errors}</div>
