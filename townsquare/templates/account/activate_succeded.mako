<%inherit file="/base.mako"/>\

<%def name="header()">Editing ${c.title}</%def>

<h1>Account Activation Success</h1>

<div>
<p>You have successfully activated your account.
</div>