<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
  <head>
    <title>TownSquare</title>
    ${h.stylesheet_link('/quick.css')}
        <script src="/js/jquery.js" type="text/javascript" charset="utf-8"></script>
  </head>

  <body>
    <div class="page-top">
      <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/channels">Channels</a></li>
        <li><a href="/users">People</a></li>
        <li><a href="/register">Register</a></li>
        <li><a href="/login">Login</a></li>
        <li><a href="/logout">Logout</a></li>
      </ul>
    </div>
    <div class="content">

      ${next.body()}

    </div>
  </body>
</html>
