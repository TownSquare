<%inherit file="/base.mako"/>

<h1>People</h1>

% if c.item_list.count() > 0:
<ul>
% for p in c.item_list:
  <li><a href="${p.url}">${p.name}</a></li>
% endfor
</ul>
% endif
