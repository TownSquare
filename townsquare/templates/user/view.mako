<%inherit file="/base.mako"/>

<div>
<h1>User: ${h.literal(c.user.name)}</h1>
</div>

<div>
${h.secure_form(url('/posts/create'))}
  <div>${h.textarea(name='content', rows=7, cols=40, content=c.content)}</div>
  <div>
    ${h.submit(value='Post', name='commit')}
    ${h.hidden(name='content_type', value='text')}
    ${h.hidden(name='host_classname', value=c.user.__class__.__name__)}
    ${h.hidden(name='host_id', value=c.user._id)}
    ${h.hidden(name='redir', value=c.user.url)}
  </div>
${h.end_form()}
</div>

<div class="wall">
<%
  c.post_list = c.user.posts
  c.current_url = c.user.url
%>
<%include file="/post/list-embed.mako"/>
</div>
