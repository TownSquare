<%inherit file="/base.mako"/>

<h1>Discussions</h1>

% if c.item_list.count() > 0:
<ul>
% for p in c.item_list:
  <li><div><strong><a href="${p.url}">${p.name}</strong></div></a>
% if p.channels.count() > 0:
channels:
% for ch in p.channels:
 <a href="${ch.url}">${ch.name}</a>
% endfor
% else:
<em>Not posted to any channel</em>
% endif
  </li>
% endfor
</ul>
% endif
