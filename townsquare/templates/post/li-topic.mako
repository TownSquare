
  <div class="avatar"><a href="${c.post_li.creator.url}"><img src="/post_li-topic.png" width="32" height="32" /></a></div>
  <div class="block">
  
% if hasattr(c.post_li, 'name') and len(c.post_li.name):
  <big><a href="${c.post_li.url}" title="permalink">${c.post_li.name}</a></big>
% endif
  <div class="content">
% if hasattr(c.post_li, 'deletion_status') and c.post_li.deletion_status is True:
% if hasattr(c.post_li, 'deletion_timestamp'):
  <p class="lo-contrast"><em>Post deleted at ${c.post_li.deletion_timestamp}</em></p>
% else:
  <p class="lo-contrast"><em>Post deleted</em></p>
% endif
% endif

  </div>
  <!--div class="details"><small><span class="author"><strong><a href="${c.post_li.creator.url}">${h.literal(c.post_li.creator.name)}</a></strong></span> &bull; <a href="${c.post_li.url}" title="permalink">${c.post_li.creation_timestamp}</a>
  &bull <a href="${c.post_li.url}/edit?redir=${c.current_url}">edit</a>
  &bull <a href="${c.post_li.url}/delete?redir=${c.current_url}">delete</a>
  </small></div-->
  <div class="details"><small>
% if c.post_li.direct_response_count() > 0:
  <a href="">${c.post_li.direct_response_count()} direct responses, ${c.post_li.total_response_count()} responses in total</a>, last response at ${c.post_li.creation_timestamp}
% else:
  <a href="${c.post_li.url}">add response</a>
% endif
  </small></div>
  </div>
