<%inherit file="/base.mako"/>

<h1>Post</h1>

<div>
${h.secure_form(url(''))}
<dl>
% if c.new_post or (c.title and len(c.title)):
  <dt>Title</dt>
  <dd>
    ${h.text(name='title', value=c.title, maxLength=255)}
  </dd>
% endif
  <dt>Content</dt>
  <dd>
    ${h.textarea(name='content', rows=7, cols=40, content=c.content)} <br />
  </dd>
  <dt>&nbsp;</dt>
  <dd>
    ${h.submit(value='Save changes', name='commit')}
% if c.content_type and len(c.content_type):
    ${h.hidden(name='content_type', value=c.content_type)}
% endif
% if c.post_id and len(c.post_id):
    ${h.hidden(name='post_id', value=c.post_id)}
% endif
% if c.redir and len(c.redir):
    ${h.hidden(name='success_redirect', value=c.redir)}
% endif
% if len(c.host_classname) and len(c.host_id):
    ${h.hidden(name='host_classname', value=c.host_classname)}
    ${h.hidden(name='host_id', value=c.host_id)}
% endif
  </dd>
</dl>
${h.end_form()}
</div>
