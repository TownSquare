<%inherit file="/base.mako"/>

<div class="page-nav">
%   if c.post.get_parent() is not None:
<a href="${c.post.get_parent().url}">Go to parent post</a>
%   endif
%   if c.post.get_host() is not None:
<a href="${c.post.get_host().url}">Go to ${c.post.get_host().name}</a>
%   endif
</div>

<div class="post">

<div class="content">
% if hasattr(c.post, 'name') and len(c.post.name) is not 0:
<h1>${c.post.name}</h1>
% endif
<div>
${h.literal(h.markdown.markdown(c.post.content, safe_mode="escape"))}
</div>
</div>

% if c.post.creator is not None:
<div class="avatar"><img src="/xib_normal.png" width="32" height="32" /></div>
<div class="block details"><small>Posted by <strong><a href="${c.post.creator.url}">${c.post.creator.name}</a></strong> at ${c.post.creation_timestamp}
%   if c.post.get_parent() is not None:
as response to <a href="${c.post.get_parent().url}">a post</a>
%   endif
%   if c.post.get_host() is not None:
on <a href="${c.post.get_host().url}">${c.post.get_host().name}</a>
%   endif
.
<br />${c.post.direct_response_count()} direct responses, 25 responses in total, 5 reposts, 2 referencing posts, 10 likes
</small></div>
<div class="clear"></div>
% endif

</div>

<div class="responses">
<h3>Responses:</h3>
<%
  c.post_list = c.post.children
  c.current_url = c.post.url
%>
<%include file="list-embed.mako"/>
</div>


<!--TODO: channel-->
<div>
${h.secure_form(url('/posts/create'))}
  <div>${h.textarea(name='content', rows=7, cols=40, content=c.content)}</div>
  <div>
    ${h.submit(value='Post', name='commit')}
    ${h.hidden(name='parent_id', value=c.post._id)}
    ${h.hidden(name='redir', value=c.post.url)}
    ${h.hidden(name='content_type', value='text')}
  </div>
${h.end_form()}
</div>
