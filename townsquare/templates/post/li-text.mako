
  <div class="avatar"><a href="${c.post_li.creator.url}"><img src="/xib_normal.png" width="24" height="24" /></a></div>
  <div class="block">
  
  <div class="details"><small><span class="author"><strong><a href="${c.post_li.creator.url}">${h.literal(c.post_li.creator.name)}</a></strong></span> &bull; <a href="${c.post_li.url}" title="permalink">${c.post_li.creation_timestamp}</a>
  &bull <a href="${c.post_li.url}/edit?redir=${c.current_url}">edit</a>
  &bull <a href="${c.post_li.url}/delete?redir=${c.current_url}">delete</a>
  </small></div>
  <div class="content">
% if hasattr(c.post_li, 'name') and len(c.post_li.name):
  <big>${c.post_li.name}</big>
% endif
% if hasattr(c.post_li, 'deletion_status') and c.post_li.deletion_status is True:
% if hasattr(c.post_li, 'deletion_timestamp'):
  <p class="lo-contrast"><em>Post deleted at ${c.post_li.deletion_timestamp}</em></p>
% else:
  <p class="lo-contrast"><em>Post deleted</em></p>
% endif
% else:
  ${h.literal(h.markdown.markdown(c.post_li.content, safe_mode="escape"))}
% endif

  </div>
  <div class="details"><small><a href="">${c.post_li.direct_response_count()} direct responses, 25 responses in total, 10 likes</a></small></div>
  </div>
