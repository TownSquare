
% if c.post_list.count() > 0:
<div class="post-list">
<ul>
% for p in c.post_list:
<li class="post" id="post_${p._id}">


<%
  c.post_li = p
  if not hasattr(p, 'content_type'):
    p.content_type = 'text'
%>
% if p.content_type == 'text':
<%include file="li-text.mako"/>
% elif p.content_type == 'topic':
<%include file="li-topic.mako"/>
% elif p.content_type == 'article':
<%include file="li-article.mako"/>
% endif

  <div class="clear"></div>
</li>
% endfor
</ul>
</div>
% else:
No responses yet.
% endif
