import logging

from pylons import request, response, session, tmpl_context as c, app_globals as g
from pylons.controllers.util import abort, redirect_to

from fma.orm import mapper
from townsquare.model.post import Post
from townsquare.model.channel import Channel
from townsquare.model import user
from townsquare.lib.base import BaseController, render

log = logging.getLogger(__name__)

#mapper(Channel, Post, account.Account)

class ChannelController(BaseController):

    def view(self, idname):
        c.ch = g.db.col(Channel).find_one(_id=idname)
        #TODO: 404 if not found
        #TODO: pagination
        wallposts = g.db.col(Post).find(host_classname='Channel', host_id=c.ch._id)
        order = {'_id':-1}
        c.ch.posts = wallposts.sort(**order)
        return render('/channel/view.mako')

    def list(self):
        c.item_list = g.db.col(Channel).find()
        return render('/channel/list.mako')
        
    def pick_single(self):
        """
        """
        #TODO: use the parameter 'return'
        # use COOKIE to store selection
        return 'eee'
    
    def pick_multi(self):
        return 'aaa'

    def create(self):
        #TODO: check settings, role and acl
        u = user.get_current_user();
        if u is None:
            redirect_to(user.create_login_url(''))
        c.field_errors = []
        form_valid = False
        if (len(request.POST)):
            c.title = request.POST.getone('title').strip()
            if len(c.title) == 0:
                c.field_errors.append('title')
            c.content = request.POST.getone('content').strip()
            if len(c.content) == 0:
                c.field_errors.append('content')
            form_valid = len(c.field_errors) == 0
        
        if form_valid:
            nd = g.db.col(Channel).find().count()
            disc = g.db.col(Channel).new()
            disc.creator_id = u.id
            disc.name = c.title
            #TODO: create slug
            disc.idname = "channel_%i" % nd
            disc.description = c.content
            disc.save()
            #TODO: redirect to the newly created channel
            redirect_to(disc.url)
        
        return render('/channel/edit.mako')
