import logging

from pylons import request, response, session, tmpl_context as c, app_globals as g
from pylons.controllers.util import abort, redirect_to

from fma.orm import mapper
from townsquare.model.post import Post
from townsquare.model.channel import Channel
from townsquare.model import user
from user import User
from townsquare.lib.base import BaseController, render

log = logging.getLogger(__name__)

#mapper(Channel, Post, account.Account)

class PostController(BaseController):

    def view(self, idname):
        c.post = g.db.col(Post).find_one(_id=idname)
        if c.post is None:
            return render('/post/404.mako')
        cs = g.db.col(Post).find(parent_id=c.post._id)
        #cs.sort('_id')
        #c.post.children = g.db.col(Post).find(parent_id=c.post._id)
        order = {'_id':1}
        c.post.children = cs.sort(**order)
        return render('/post/view.mako')

    def list(self):
        c.item_list = g.db.col(Post).find()
        return render('/post/list.mako')

    def edit(self, idname=None):
        #TODO: detect content_type based on properties
        # oneliner: content only, one line
        # text: content only, for replies
        # article: title is a must, optional brief and single pic
        # question: title is a must, optional content as description
        # link: title is a must, optional content as description
        
        #TODO: check settings, role and acl
        u = user.get_current_user();
        if u is None:
            redirect_to(user.create_login_url(''))
        
        c.new_post = False
        
        if idname is None or len(idname) is 0:
            c.new_post = True
            try: c.content_type = request.GET.getone('type').strip()
            except:
                #TODO: show as internal error
                pass
            try: c.host_classname = request.GET.getone('host_classname').strip()
            except: pass
            try: c.host_id = request.GET.getone('host_id').strip()
            except: pass
        
        c.field_errors = []
        form_valid = False
        
        if len(request.POST):
            #CHECK: mandatory?
            try: c.content_type = request.POST.getone('content_type').strip()
            except: pass
            #TODO: show message if the post has no content property
            c.content = request.POST.getone('content').strip()
            if len(c.content) == 0:
                c.field_errors.append('content')
            try: c.title = request.POST.getone('title').strip()
            except: pass
            #TODO: This depends on the content type or this affects the content type
            #if len(c.title) == 0:
            #    c.field_errors.append('title')
            try: c.host_classname = request.POST.getone('host_classname').strip()
            except: pass
            try: c.host_id = request.POST.getone('host_id').strip()
            except: pass
            try: c.parent_id = request.POST.getone('parent_id').strip()
            except: pass
            try: c.post_id = request.POST.getone('post_id').strip()
            except: pass
            
            try: c.redir = request.POST.getone('redir').strip()
            except: pass
            
            form_valid = len(c.field_errors) == 0
        
        if form_valid:
            
            if c.post_id and len(c.post_id) > 0:
                # Editing existing post
                
                post = g.db.col(Post).find_one(_id=idname)
                #TODO: catch exceptions
                
            else:
                # Creating new post
                
                post = g.db.col(Post).new()
                post.content_type = c.content_type
                post.creator_id = u.id
                
                if len(c.title):
                    post.name = c.title
                                
                if len(c.host_classname) and len(c.host_id):
                    cl = globals()[c.host_classname]
                    #TODO: catch exception
                    host = g.db.col(cl).find_one(_id=c.host_id)
                    #TODO: check acl and role
                    if host is not None:
                        post.host = host
                        
                if len(c.parent_id):
                    post_parent = g.db.col(Post).find_one(_id=c.parent_id)
                    if post_parent is not None:
                        post.parent = post_parent
                
            post.content = c.content
            
            #TODO: catch exceptions
            post.save()
            
            if len(c.redir):
                redirect_to(str(c.redir))
                
            #TODO: message or the post?
            redirect_to(post.url)
            
            #TODO: better notification message
            return "Saved"

        if idname and len(idname) is not 0:
            p = g.db.col(Post).find_one(_id=idname)
            #TODO: catch exceptions
            try: c.content_type = p.content_type
            except: pass
            c.post_id = str(p._id)
            c.content = p.content
            try: c.title = p.name
            except: pass
            try: c.redir = request.GET.getone('redir')
            except: pass
        
        return render('/post/edit.mako')
        
    def delete(self, idname):
        #TODO: check settings, role and acl
        u = user.get_current_user();
        if u is None:
            redirect_to(user.create_login_url(''))
            
        p = g.db.col(Post).find_one(_id=idname)
        #TODO: catch exceptions
        
        p.delete(u.id)
        
        return 'delete %s' % str(idname)
        
