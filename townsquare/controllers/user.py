import logging

from pylons import request, response, session, tmpl_context as c, app_globals as g
from pylons.controllers.util import abort, redirect_to

from fma.orm import mapper
from townsquare.model.post import Post
from townsquare.model.user import User
from townsquare.lib.base import BaseController, render

log = logging.getLogger(__name__)

#mapper(User, Discussion)

class UserController(BaseController):

    def view(self, idname):
        c.user = g.db.col(User).find_one(idname=idname)
        wallposts = g.db.col(Post).find(host_classname='User', host_id=c.user._id)
        order = {'_id':-1}
        c.user.posts = wallposts.sort(**order)
        return render('/user/view.mako')

    def list(self):
        c.item_list = g.db.col(User).find()
        return render('/user/list.mako')
