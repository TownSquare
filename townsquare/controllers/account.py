import logging, re

from pylons import request, response, session, tmpl_context as c, app_globals as g
from pylons.controllers.util import abort, redirect_to

from townsquare.model import user
from townsquare.lib import mail
from townsquare.lib.base import BaseController, render

log = logging.getLogger(__name__)

class AccountController(BaseController):

    def login(self):
        # Forward to home if logged in
        accid = session.get('account.id')
        if accid is not None and len(accid):
            redirect_to('/') #TODO: application's root, not host's root
        c.field_errors = []
        form_valid = False
        if len(request.POST):
            c.username = request.POST.getone('username').strip()
            idname = c.username.lower()
            acc = g.db.col(user.User).find_one(idname=idname)
            if acc is not None:
                if acc.authenticate(request.POST.getone('password')):
                    #TODO: should use session id
                    session['account.id'] = str(acc._id)
                    session.save()
                    #TODO: show message (with auto-redirect) or just redirect
                    redirect_to('/') #TODO: application's root, not host's root
        return render('/account/login.mako')

    def logout(self):
        session['account.id'] = ''
        session.save()
        redirect_to('/') #TODO: application's root, not host's root

    def register(self):
        # Forward to home if logged in
        accid = session.get('account.id')
        if accid is not None and len(accid):
            redirect_to('/') #TODO: application's root, not host's root
        c.title = 'Eaaaa'
        c.field_errors = []
        form_valid = False
        if len(request.POST):
            # Process submited form data here
            #TODO: lots of check here
            c.username = request.POST.getone('username').strip()
            if (len(c.username) < 4 or len(c.username) > 25):
                c.field_errors.append('username')
            if (re.match(r'^[A-Za-z0-9_]+$', c.username) == None):
                c.field_errors.append('username')
            c.email = request.POST.getone('email')
            if (len(c.email) < 4):
                c.field_errors.append('email')
            c.email_confirm = request.POST.getone('email_confirm')
            if (c.email_confirm != c.email or len(c.email_confirm) < 4):
                c.field_errors.append('email_confirm')
            c.password = request.POST.getone('password')
            if (len(c.password) < 6):
                c.field_errors.append('password')
            c.password_confirm = request.POST.getone('password_confirm')
            if (c.password_confirm != c.password or len(c.password_confirm) < 6):
                c.field_errors.append('password_confirm')
            #TODO: custom fields (example: invitation code, referrer, introductory message)
            form_valid = len(c.field_errors) == 0
            
        if form_valid:
            # Continue the registration
            u = g.db.col(user.User).new()
            u.name = c.username
            u.email = c.email
            u.passkey = c.password
            #u.admin = True
            vals_valid = False
            try:
                #user.save()
                g.db.col(user.User).insert(u)
            except user.NameExists:
                c.field_errors.append('username')
            except user.EmailExists:
                c.field_errors.append('email')
                c.field_errors.append('email_confirm')
            else:
                pass
            vals_valid = len(c.field_errors) == 0
            if vals_valid:
                #TODO: prepare the content
                mail.send_administration_mail(u.email, "Activation Code", "TODO: mail message here!")
                
                #TODO: redirect
                return render('/account/register_succeded.mako')
            
        return render('/account/register.mako')
    
    def activate(self, key=None):
        accid = session.get('account.id')
        if accid is not None and len(accid):
            redirect_to('/') #TODO: application's root, not host's root
        if (len(request.POST)):
            key = request.POST.getone('activation_key').strip()
        if key is not None:
            u = g.db.col(user.User).find_one(activation_key=key)
            if u is not None:
                if u.activate(key):
                    u.save()
                    return render('/account/activate_succeded.mako')
            #TODO: fail message
        return render('/account/activate.mako')
        
    def invite(self):
        accid = session.get('account.id')
        if accid is None or len(accid) == 0:
            redirect_to('/login') #TODO: application's root, not host's root. with redirect parameter.
        #TODO: check config (whom can invite, the number of invitations)
        #TODO: check session
        return render('/account/invite.mako')
